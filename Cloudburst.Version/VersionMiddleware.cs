using System;
using System.Text.Json;
using System.Threading.Tasks;
using Cloudburst.Version.Abstractions;
using Microsoft.AspNetCore.Http;

namespace Cloudburst.Version {
	public class VersionMiddleware {
		private readonly RequestDelegate _next;
		private readonly IServiceVersion _serviceVersion;

		public VersionMiddleware(RequestDelegate next, IServiceVersion serviceVersion) {
			_next = next ?? throw new ArgumentNullException(nameof(next));
			_serviceVersion = serviceVersion ?? throw new ArgumentNullException(nameof(serviceVersion));
		}

		public async Task InvokeAsync(HttpContext httpContext) {
			httpContext.Response.StatusCode = 200;
			httpContext.Response.ContentType = "application/json; charset=utf-8";

			var version = new JsonVersionResponse(_serviceVersion);

			string serializedVersion = JsonSerializer.Serialize(version, new JsonSerializerOptions {
				WriteIndented = true
			});

			await httpContext.Response.WriteAsync(serializedVersion);
		}
	}
}
