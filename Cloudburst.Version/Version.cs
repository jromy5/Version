using System;
using System.Text;
using Cloudburst.Version.Abstractions;

namespace Cloudburst.Version {
	public abstract class Version : IServiceVersion {
		public virtual int Major { get; } = 0;
		public virtual int Minor { get; } = 1;
		public virtual int Patch { get; } = 0;
		public virtual string PreRelease { get; } = "";
		public virtual int Build { get; } = 0;

		public Guid UniqueInstanceIdentifier { get; } = Guid.NewGuid();

		protected Version() {
			if (Major < 0 || Minor < 0 || Patch < 0) {
				throw new ArgumentOutOfRangeException("Version cannot contain negative numbers.");
			}
		}

		public string FullVersion() {
			var stringBuilder = new StringBuilder();
			stringBuilder.AppendFormat("{0}.{1}.{2}", Major, Minor, Patch);

			if (!String.IsNullOrEmpty(PreRelease)) {
				stringBuilder.AppendFormat("-{0}", PreRelease);
			}

			if (Build > 0) {
				stringBuilder.AppendFormat("+{0}", Build);
			}

			return stringBuilder.ToString();
		}

		public override string ToString() {
			var stringBuilder = new StringBuilder();
			stringBuilder.AppendFormat("{0}.{1}.{2}", Major, Minor, Patch);
			return stringBuilder.ToString();
		}
	}
}
