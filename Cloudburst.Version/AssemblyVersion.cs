using System.Reflection;

namespace Cloudburst.Version {
	public class AssemblyVersion : Version {
		// ReSharper disable once PossibleNullReferenceException
		private System.Version _version => Assembly.GetEntryAssembly().GetName().Version;

		public override int Major => _version.Major;

		public override int Minor => _version.Minor;

		public override int Patch => _version.Revision;

		public override string PreRelease { get; } = "";

		public override int Build => _version.Build;
	}
}
